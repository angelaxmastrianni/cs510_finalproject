import pandas
from keras.models import Sequential
from keras.layers import Dense, np
from sklearn.metrics import confusion_matrix
import seaborn as sn
import matplotlib.pyplot as plt

from PrepareData import get_data

nonuniversity_subreddits = [ "investing", "AskNYC", "thingsmykidsaid", "nfl", "help"]
university_subreddits = [ "cmu", "Drexel", "Temple", "UPenn", "villanova"]

train_input, validate_input, test_input, train_output, validate_output, test_output = get_data("university_data/")

# define baseline model
def baseline_model():
	# create model
	model = Sequential()
	model.add(Dense(512, input_dim=1000, activation='softmax'))
	model.add(Dense(5, activation='softmax'))
	# Compile model
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	return model

model = baseline_model()

model.fit(train_input,train_output, validation_data = (validate_input,validate_output),
epochs=100, batch_size=10)

predicted_output = model.predict_classes(test_input)
ground_truth = np.argmax(test_output, axis=1)
confusion_matrx = confusion_matrix(ground_truth, predicted_output)

result = model.evaluate(test_input,test_output)

for i in range(len(model.metrics_names)):
	print("Metric ",model.metrics_names[i],":",
	str(round(result[i],2)))

plt.figure(figsize = (10,7))
sn.heatmap(confusion_matrx, annot=True,annot_kws={"size": 12}, xticklabels=university_subreddits, yticklabels=university_subreddits)
b, t = plt.ylim() # discover the values for bottom and top
b += 0.5 # Add 0.5 to the bottom
t -= 0.5 # Subtract 0.5 from the top
plt.ylim(b, t) # update the ylim(bottom, top) values
plt.show() # ta-da!

