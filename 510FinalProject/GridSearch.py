
from keras import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import  GridSearchCV
from PrepareData import *

university_subreddits = [ "cmu", "Drexel", "Temple", "UPenn", "villanova"]
nonuniversity_subreddits = [ "investing", "AskNYC", "thingsmykidsaid", "nfl", "help"]

# define baseline model
def baseline_model(activation):
    # create model
    model = Sequential()
    model.add(Dense(512, input_dim=1000, activation=activation))
    model.add(Dense(5, activation=activation))
    # Compile model
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

model = KerasClassifier(build_fn=baseline_model, epochs=10, batch_size=10, verbose=0)

activation = ['softmax', 'softplus', 'sigmoid', 'hard_sigmoid' ]
batch_size = [10, 50, 100]
epochs = [10, 50, 100]
param_grid = dict(batch_size=batch_size, epochs=epochs, activation=activation)
grid = GridSearchCV(estimator=model, param_grid=param_grid, n_jobs=-1, cv=3)

train_input, validate_input, test_input, train_output, validate_output, test_output = get_data("nonuniversity_data/")
grid_result = grid.fit(train_input, train_output)

print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))