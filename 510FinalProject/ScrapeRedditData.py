import praw

university_subreddits = [ "CMU", "drexel", "temple", "upenn", "villanova"]
nonuniversity_subreddits = [ "help"]

reddit = praw.Reddit(client_id='nHerjXF-yNTKsQ',
                     client_secret='oKMRtxajDMfBX-2C5ITCsCdOylI',
                     user_agent='script:cs510project:1.0.9 (by /u/moneydiariesobsessed)')

def scrapeSubreddit(subreddit_name, file_directory):
    count = 0
    authors = {}
    for submission in reddit.subreddit(subreddit_name).new(limit=100000):
        if 0 < len(submission.selftext) < 201  and "https" not in submission.selftext:
            file_name = file_directory + subreddit_name + "_" + str(count) + ".txt"
            file = open(file_name, "w")
            title = submission.title + "\n"
            author = str(submission.author) + "\n"
            posted_subreddits = []
            for user_submission in reddit.redditor(str(submission.author)).submissions.top('all'):
                if submission.title is not user_submission.title:
                    posted_subreddits.append(str(user_submission.subreddit))
            if (author in authors):
                freq = authors[author]
                authors[author] = freq + 1
            else:
                authors[author] = 1
            text = submission.selftext + "\n"
            file.writelines([title, author, text, ",".join(set(posted_subreddits))])
            count = count + 1
    print(subreddit_name + ":" + str(count))

#for university_subreddit in university_subreddits:
   # scrapeSubreddit(university_subreddit, "university_data/")

for nonuniversity_subreddit in nonuniversity_subreddits:
     scrapeSubreddit(nonuniversity_subreddit, "nonuniversity_data/")