Places where no one can hear me?
ros_lux
I'm undergoing voice training so every day I have to practice some weird-sounding vocal exercises. What are some good spots on campus where I can be alone and people can't hear me?
menwritingwomen,meettransgirls,actuallesbians,Kaguya_sama,me_irlgbt,2meirl4meirl,cmu,APStudents,BreadTube,egg_irl,traaaaaaannnnnnnnnns