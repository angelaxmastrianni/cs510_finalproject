import os

from keras.utils import np_utils
from scipy.sparse import csr_matrix
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder


def get_lines(folder_name, docs, labels):
    posted_subreddits = []
    for filename in os.listdir(folder_name):
        f = open(folder_name + filename, "r")
        lines = f.readlines()
        docs.append(lines[2])
        if len(lines) > 3:
            posted_subreddits.append(lines[3])
        else:
            posted_subreddits.append("")
        f.close()
        labels.append(filename.split("_")[0])
    return posted_subreddits

def get_data(folder_name, authorship_subreddits = None):
    docs = []
    labels = []
    authorship_data = []
    posted_subreddits = get_lines(folder_name, docs, labels)
    #
    # encoder = LabelEncoder()
    # encoder.fit(labels)
    # encoded_Y = encoder.transform(labels)
    #
    # #convert integers to dummy variables (i.e. one hot encoded)
    # labels = np_utils.to_categorical(encoded_Y)

    if authorship_subreddits is None:
        return get_non_authorship_data(docs, labels)

    for subredditList in posted_subreddits:
        subreddits = subredditList.split(",")
        authorship = []
        for subreddit in authorship_subreddits:
            if subreddit in subreddits:
                authorship.append(1)
            else:
                authorship.append(0)
        authorship_data.append(authorship)

    return get_authorship_data(docs, labels, authorship_data)



def get_non_authorship_data(docs, labels):

    x_train, x_test, train_output, test_output = train_test_split(docs, labels, test_size=0.2, random_state=2018,
                                                        stratify=labels)
    x_train, x_val, train_output, validate_output = train_test_split(x_train, train_output, test_size=0.1, random_state=2018,
                                                      stratify=train_output)

    vectorizer = TfidfVectorizer(max_features=1000)
    input_vectors = vectorizer.fit_transform(x_train)
    validate_vectors = vectorizer.transform(x_val)
    test_vectors = vectorizer.transform(x_test)

    return input_vectors, validate_vectors, test_vectors, train_output, validate_output, test_output

def get_authorship_data(docs, labels, authorship):
    all_input_data = []
    count = 0
    while count < len(docs):
        input_data = (docs[count], authorship[count])
        all_input_data.append(input_data)
        count = count + 1

    x_train, x_test, train_output, test_output = train_test_split(all_input_data, labels, test_size=0.2, random_state=2018,
                                                        stratify=labels)
    x_train, x_val, train_output, validate_output = train_test_split(x_train, train_output, test_size=0.1, random_state=2018,
                                                      stratify=train_output)

    train_docs = [x[0] for x in x_train]
    validate_docs = [x[0] for x in x_val]
    test_docs = [x[0] for x in x_test]

    vectorizer = TfidfVectorizer(max_features=1000)
    input_vectors = vectorizer.fit_transform(train_docs).toarray()
    validate_vectors = vectorizer.transform(validate_docs).toarray()
    test_vectors = vectorizer.transform(test_docs).toarray()

    all_train_data = []

    count = 0
    while count < len(x_train):
        vector_list = input_vectors[count].tolist()
        vector_list.extend(x_train[count][1])
        all_train_data.append(vector_list)
        count = count + 1

    all_validate_data = []
    count = 0
    while count < len(x_val):
        vector_list = validate_vectors[count].tolist()
        vector_list.extend(x_val[count][1])
        all_validate_data.append(vector_list)
        count = count + 1

    all_test_data = []
    count = 0
    while count < len(x_test):
        vector_list = test_vectors[count].tolist()
        vector_list.extend(x_test[count][1])
        all_test_data.append(vector_list)
        count = count + 1

    return csr_matrix(all_train_data), csr_matrix(all_validate_data), csr_matrix(all_test_data), train_output, validate_output, test_output