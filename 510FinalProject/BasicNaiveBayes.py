from sklearn import svm
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.naive_bayes import MultinomialNB
import seaborn as sn
import matplotlib.pyplot as plt

from PrepareData import get_data

university_subreddits = [ "CMU", "drexel", "temple", "upenn", "villanova"]
nonuniversity_subreddits = [ "investing", "AskNYC", "thingsmykidsaid", "nfl", "help"]


train_input, validate_input, test_input, train_output, validate_output, test_output = get_data("university_data/")

model = MultinomialNB()
print("Training model.")

#train model
model.fit(train_input, train_output)
predicted_labels = model.predict(test_input)

confusion_matrx = confusion_matrix(test_output, predicted_labels)
print("FINISHED classifying. accuracy score : ")
print(accuracy_score(test_output, list(predicted_labels)))

plt.figure(figsize = (10,7))
sn.heatmap(confusion_matrx, annot=True,annot_kws={"size": 12}, xticklabels=university_subreddits, yticklabels=university_subreddits)
b, t = plt.ylim() # discover the values for bottom and top
b += 0.5 # Add 0.5 to the bottom
t -= 0.5 # Subtract 0.5 from the top
plt.ylim(b, t) # update the ylim(bottom, top) values
plt.show() # ta-da!


